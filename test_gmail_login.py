
import time

import unittest2 as unittest
import xmlrunner
import HtmlTestRunner
from selenium import webdriver


class TestLogin(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        capabilities = {'browserName': 'chrome', 'javascriptEnabled': True}
        command_executor = 'http://principia01.hukot.net:4444/wd/hub'
        # cls.driver = webdriver.Chrome(chrome_options=options)
        cls.driver = webdriver.Remote(command_executor=command_executor, desired_capabilities=capabilities, options=options)

    def test_login(self):
        driver = self.driver
        driver.get("https://accounts.google.com/signin")
        self.assertIn("Google", driver.title)
        user_name = "mistrselenium@gmail.com"
        password = "Mtest12345"

        email_elem = driver.find_element_by_css_selector('input[name="Email"]')
        email_elem.send_keys(user_name)
        driver.find_element_by_css_selector('input[name="signIn"]').click()
        time.sleep(5)

        pswd_elem = driver.find_element_by_css_selector('input[name="Passwd"]')
        pswd_elem.send_keys(password)
        driver.find_element_by_css_selector('input[name="signIn"]').click()
        time.sleep(5)

        current_url = driver.current_url
        expected_url = 'https://myaccount.google.com/?utm_source=sign_in_no_continue&pli=1'
        # assert url
        self.assertEqual(current_url, expected_url)

        # logout
        logout = driver.find_element_by_css_selector('span[class="gb_Ba gbii"]')
        logout.click()
        signout = driver.find_element_by_css_selector('a#gb_71')
        signout.click()
        time.sleep(5)
        user_button = driver.find_element_by_css_selector('button[type="submit"]')
        user_value = user_button.get_attribute("value")
        # # logout assert
        self.assertEqual(user_value, user_name)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()


if __name__ == '__main__':
    unittest.main(verbosity=2)
    # unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'), failfast=False, buffer=False, catchbreak=False)
    # unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='test-reports'), failfast=False, buffer=False, catchbreak=False)