# -*- coding: utf-8 -*-
import time

import unittest as unittest
import xmlrunner
# import HtmlTestRunner
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestLogin(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        capabilities = {'browserName': 'chrome', 'javascriptEnabled': True}
        command_executor = 'http://principia01.hukot.net:4444/wd/hub'
        cls.driver = webdriver.Remote(command_executor=command_executor, desired_capabilities=capabilities, options=options)

    def test_login(self):
        driver = self.driver
        driver.get("https://login.szn.cz/")
        user_name = "mistrselenium@seznam.cz"
        password = "Mtest12345"
        # driver.implicitly_wait(20)
        wait = WebDriverWait(driver, 20)

        email_elem = driver.find_element_by_css_selector('input[id="login-username"]')
        email_elem.send_keys(user_name)
        wait.until(
            EC.text_to_be_present_in_element_value((By.CSS_SELECTOR, 'input[id="login-username"]'), 'mistrselenium'))

        pswd_elem = driver.find_element_by_css_selector('input[id="login-password"]')
        pswd_elem.send_keys(password)
        wait.until(
            EC.text_to_be_present_in_element_value(
                (By.CSS_SELECTOR, 'input[id="login-password"]'), password), message='Correct password was not writen')

        driver.find_element_by_css_selector('button[data-locale="login.submit"]').click()

        # logout
        wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, 'button[class="popup users plain"]')))
        logout = driver.find_element_by_css_selector('button[class="popup users plain"]')
        logout.click()

        wait.until(EC.visibility_of_element_located(
            (By.CSS_SELECTOR, 'div[role="menu"]')), message='User menu was not visible')
        menu = driver.find_element_by_css_selector('div[role="menu"]')
        signout = menu.find_element_by_css_selector('a[href^="/logout"]')
        signout.click()
        # time.sleep(5)

        logout_url = "https://login.szn.cz/"
        # # # logout assert
        self.assertEqual(driver.current_url, logout_url)

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()


if __name__ == '__main__':
    # unittest.main(verbosity=2)
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'), failfast=False, buffer=False, catchbreak=False)
    # unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='test-reports'), failfast=False, buffer=False, catchbreak=False)